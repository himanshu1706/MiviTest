package com.mivi.mivitest;

import android.app.Application;
import android.content.Context;

/**
 * Created by himanshurathore on 21/6/18.
 */

public class MiviApp extends Application {

    public static MiviApp instance = null;

    public static MiviApp getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            instance = this;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
