package com.mivi.mivitest.utils;

import com.mivi.mivitest.MiviApp;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by himanshurathore on 21/6/18.
 */

public class ProjectUtils {

    public static String loadJSONFromAsset(String fileName) {
        String json = null;
        try {
            InputStream is = MiviApp.getInstance()
                    .getAssets()
                    .open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
