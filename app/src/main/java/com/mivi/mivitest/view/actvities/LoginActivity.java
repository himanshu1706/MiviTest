package com.mivi.mivitest.view.actvities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;


import com.mivi.mivitest.R;

/**
 * Created by himanshurathore on 21/6/18.
 */

public class LoginActivity extends BaseActivity {

    private static final String SCREEN_NAME = "Login";
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setUpUI();
    }

    public void setUpUI(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setUpToolBar(toolbar, SCREEN_NAME);
    }
}
