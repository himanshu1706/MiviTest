package com.mivi.mivitest.view.actvities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

/**
 * Created by himanshurathore on 21/6/18.
 */

public class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setUpToolBar(Toolbar toolBar, String title){
        setSupportActionBar(toolBar);
        getSupportActionBar().setTitle(title);

     //   getSupportActionBar().setDisplayHomeAsUpEnabled(true);
     //   getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
    }
}
